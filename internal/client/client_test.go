package foo_test

import (
	"fooApi/internal/client"
	"testing"
	"github.com/stretchr/testify/suite"
	"github.com/pact-foundation/pact-go/dsl"
	"net/http"
	"fmt"
	"errors"
	"encoding/json"
)

type FooClientSuite struct{
	suite.Suite
}

func TestFooClientSuite(t *testing.T) {
	suite.Run(t , new(FooClientSuite))
}

var (
	pact *dsl.Pact
)

func (s *FooClientSuite) SetupSuite(){
	pact = createPactConfig("FooClient", "BarApi")
}

func createPactConfig(consumer, provider string) *dsl.Pact{
	return &dsl.Pact{
		Consumer: consumer,
		Provider: provider,
		PactDir: "./pacts",
	}
}

func (s *FooClientSuite) TearDownSuite() {
	pact.Teardown()
}


func (s *FooClientSuite) TestWhenBodyIsSyntacticallyValidButContentIsInvalid() {

	test:= func() error{
		 c := foo.NewClient(fmt.Sprintf("http://localhost:%d", pact.Server.Port),"/ping")
		 je := c.GetPongs(`{"times":-1}`)

		 if *je != foo.ResponseWhenBodyIsSyntacticallyValidButContentIsInvalid {
			 return errors.New("error returned not as expected")
		 }
		 return nil
	}

	pact.
		AddInteraction().
		Given("When test body is syntactically valid but content is invalid").
		UponReceiving("A POST request for bar api").
		WithRequest(dsl.Request{
			Method: http.MethodPost,
			Path: dsl.String("/ping"),
			Headers: dsl.MapMatcher{
				"Content-Type":dsl.String("application/json"),
			},
			Body: map[string]int{
				"times":-1,
			},
		}).
		WillRespondWith(dsl.Response{
			Status: http.StatusUnprocessableEntity,
			Headers: dsl.MapMatcher{"Content-Type": dsl.String("application/json")},
			Body: map[string]string{
				"message":foo.JsonErrorWhenBodyIsSyntacticallyValidButContentIsInvalid.Message,
				"status":foo.JsonErrorWhenBodyIsSyntacticallyValidButContentIsInvalid.Status,
			},
		})
	s.NoError(pact.Verify(test))

}


func (s *FooClientSuite) TestWhenBodyIsSyntacticallyValidButNoKeyNamedTimes() {

	test:= func() error{
		c := foo.NewClient(fmt.Sprintf("http://localhost:%d", pact.Server.Port),"/ping")
		je:= c.GetPongs(`{"time":3}`)

		if *je != foo.ResponseWhenBodyIsSyntacticallyValidButNoKeyNamedTimes {
			return errors.New("error returned not as expected")
		}
		return nil
   }
	
	pact.
		AddInteraction().
		Given("When there is no key named times in body").
		UponReceiving("A POST request for bar api").
		WithRequest(dsl.Request{
			Method: http.MethodPost,
			Path: dsl.String("/ping"),
			Headers: dsl.MapMatcher{
				"Content-Type":dsl.String("application/json"),
			},
			Body: map[string]int{
				"time":3,
			},

		}).
		WillRespondWith(dsl.Response{
			Status: http.StatusUnprocessableEntity,
			Headers: dsl.MapMatcher{
				"Content-Type": dsl.String("application/json")},
			Body: map[string]string{
				"message":foo.JsonErrorWhenBodyIsSyntacticallyValidButNoKeyNamedTimes.Message,
				"status":foo.JsonErrorWhenBodyIsSyntacticallyValidButNoKeyNamedTimes.Status,
			},
		})
	s.NoError(pact.Verify(test))

}

func (s *FooClientSuite) TestPing(){

	test:= func() error{
		c := foo.NewClient(fmt.Sprintf("http://localhost:%d", pact.Server.Port),"/ping")
		pongs := c.GetPongs(`{"times":4}`)

		expectedPong := foo.PingResponse{
			Pongs: []string{"pong", "pong", "pong","pong"},
		}
		want, _:= json.Marshal(expectedPong)
		s.Equal(string(want),*pongs)

		return nil
   }

	pact.
		AddInteraction().
		Given("It will return pong list").
		UponReceiving("A POST request for bar api").
		WithRequest(dsl.Request{
			Method: http.MethodPost,
			Path: dsl.String("/ping"),
			Headers: dsl.MapMatcher{
				"Content-Type":dsl.String("application/json"),
			},
			Body: map[string]int{
				"times":4,
			},

		}).
		WillRespondWith(dsl.Response{
			Status: http.StatusOK,
			Headers: dsl.MapMatcher{"Content-Type": dsl.String("application/json")},
			Body: map[string][]string{
				"pongs": []string{"pong", "pong", "pong", "pong"},
			},
		})
	s.NoError(pact.Verify(test))
}