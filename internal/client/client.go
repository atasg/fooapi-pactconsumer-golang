package foo

import (
	"net/http"
	"fmt"
	"strings"
	"io/ioutil"
)

const (
	ResponseWhenBodyIsSyntacticallyValidButContentIsInvalid = `{"message":"Invalid Content - Times should be greater than 0","status":"Unprocessible Entity"}`
	ResponseWhenBodyIsSyntacticallyValidButNoKeyNamedTimes = `{"message":"Invalid Content - No key that is named times","status":"Unprocessible Entity"}`
	ResponseForBadRequest = `{"message":"Invalid JSON","status":"Bad Request"}`
	)

type JsonError struct{
	Status string `json:"status"`
	Message string `json:"message"`
}
type PingResponse struct{
	Pongs []string `json:"pongs"`
}

var (
	JsonErrorWhenBodyIsSyntacticallyValidButContentIsInvalid = JsonError{
		"Unprocessible Entity", "Invalid Content - Times should be greater than 0"}
	JsonErrorWhenBodyIsSyntacticallyValidButNoKeyNamedTimes = JsonError{
		"Unprocessible Entity", "Invalid Content - No key that is named times"}
	JsonErrorForBadRequest = JsonError{
		"Bad Request", "Invalid JSON"}
	)


func NewClient(host, path string) *Client{
	return &Client{
		HttpClient: &http.Client{},
		Host: host,
		Path: path,
	}
}

type Client struct{
	HttpClient  *http.Client
	Host string
	Path string
}


func(c *Client) GetPongs(body string) *string {

	req, _ := http.NewRequest(http.MethodPost,
		fmt.Sprintf("%s%s", c.Host,c.Path),
		strings.NewReader(body))
	req.Header.Set("Content-Type", "application/json")
	
	resp , _ := c.HttpClient.Do(req)

	respBody, _ := ioutil.ReadAll(resp.Body)
	response := string(respBody)

	return &response
}
